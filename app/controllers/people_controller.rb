get '/people' do
    @people = Person.all
    erb :"/people/index"
end

get '/people/new' do
    @person = Person.new
    erb :"/people/new"
end

post '/people' do
    if params[:birthdate] != ""
        if params[:birthdate].include?("-")
            birthdate = params[:birthdate]
        else
            birthdate = Date.strptime(params[:birthdate], "%m%d%Y") if params[:birthdate]
        end
    end
    @person = Person.create(first_name: params[:first_name], 
        last_name: params[:last_name], birthdate: params[:birthdate])
    if @person.valid?
        @person.save
        redirect "/people/#{@person.id}"
    else
        @errors = ''
        @person.errors.full_messages.each do |message|
            @errors = "#{@errors} #{message}."
        end
        erb :"people/new"
    end
end

get '/people/:id/edit' do
    @person = Person.find(params[:id])
    erb :"/people/edit"
end

put '/people/:id/edit' do
    if params[:birthdate] != ""
        if params[:birthdate].include?("-")
            birthdate = params[:birthdate]
        else
            birthdate = Date.strptime(params[:birthdate], "%m%d%Y") if params[:birthdate]
        end
    end
    @person = Person.find(params[:id])
    @person.first_name = params[:first_name]
    @person.last_name = params[:last_name]
    @person.birthdate = params[:birthdate]
    @person.save
    if @person.valid?
        @person.save
        redirect "/people/#{@person.id}"
    else
        @errors = ''
        @person.errors.full_messages.each do |message|
            @errors = "#{@errors} #{message}."
        end
        erb :"people/edit"
    end
end

get '/people/:id' do
    @person = Person.find(params[:id])
    birthdate_string = @person.birthdate.strftime("%m%d%Y")
    birth_path_number = Person.get_birth_path_number(birthdate_string)
    birth_path_description = Person.get_birth_path_description(
        birth_path_number)
    @message = "Your birth path number is #{birth_path_number}. 
        #{birth_path_description}."
    erb :"/people/show"
end

delete '/people/:id' do
    person = Person.find(params[:id])
    person.delete
    redirect "/people"
end
