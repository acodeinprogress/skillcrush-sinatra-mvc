require 'sinatra'

def setup_index_view
    @birthdate = params[:birthdate]
    birth_path_number = Person.get_birth_path_number(@birthdate)
    birth_path_description = Person.get_birth_path_description(birth_path_number)
    @message = "Your numerology number is #{birth_path_number}. 
        #{birth_path_description}"
    erb :index
end

get '/' do
    erb :form
end

post '/' do
    birthdate = params[:birthdate]
    if Person.valid_birthdate(birthdate)
        birth_path_number = Person.get_birth_path_number(birthdate)
        redirect "/message/#{birth_path_number}"
    else
        @error = 'You should enter a valid birthdate in the form of mmddyyyy.'
        erb :form
    end
end

get '/newpage' do
    erb :newpage
end

get '/:birthdate' do
    setup_index_view
end

get '/message/:birth_path_number' do
    birth_path_number = params[:birth_path_number].to_i
    birth_path_description = Person.get_birth_path_description(birth_path_number)
    @message = "Your numerology number is #{birth_path_number}. 
        #{birth_path_description}"
    erb :index
end
